<?php
/**
 * Post Sticky
 * Get sticky post for front-page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#page
 * @package WordPress
 */

$post_id = ot_get_option('sticky_post');

if($post_id != ''):

/* by ID */
$args = array(
    'p' => $post_id, // id of a page, post, or custom type
    'post_type' => 'any'
);

$the_query = new WP_Query($args);

if($the_query->have_posts()) : while( $the_query->have_posts() ) : $the_query->the_post();
?>

<div class="header-post-sticky">
	
	<span class="featured-label">STICKY POST</span>

	<?php 
		opentute_post_thumbnail(); 
		opentute_entry_header(); 
	?>

	<div class="entry-summary">
		<?php
			the_excerpt(); 
		?>
	</div><!-- .entry-summary -->
</div>

<?php endwhile; wp_reset_query(); endif; endif; ?>
