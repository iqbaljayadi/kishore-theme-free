<?php
/**
 * Post Featured
 * Get featured post for front-page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#page
 * @package WordPress
 */

$post_id = ot_get_option('featured_post');

if($post_id != ''):

/* by ID */
$args = array(
    'p' => $post_id, // id of a page, post, or custom type
    'post_type' => 'any'
);

$the_query = new WP_Query($args);

if($the_query->have_posts()) : while( $the_query->have_posts() ) : $the_query->the_post();

$thumb_src = opentute_addontag_post_thumbnail_src(get_the_ID());
$default_src = ot_get_option('featured_post_default_background');
$bg_style = '';

if(is_array($default_src))
{	
	foreach ($default_src as $key => $value):

	if($key == 'background-image') 
	{					
		if($thumb_src) {
			$value = 'url(' . $thumb_src . ')';
		} else {
			$value = 'url(' . $value . ')';
		}
	}

	$bg_style.= $key . ':' . $value . ';';

	endforeach;
}
else {
	if($thumb_src) {
		$bg_style = "background:url(" . $thumb_src . ")";	
	} else {
		$bg_style = "background:#233B54;";
	}
}

?>

<div class="header-post-featured" style="<?php echo $bg_style ?>">
	<section class="container">
		<div class="row">		
			<div class="col-md-9">
				<span class="featured-label">FEATURED</span>

				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

				<div class="meta-header">
					<?php 
						$separator = '<span class="meta-title-sparator color-white">by</span>';

						opentute_addontag_entry_date(); 
						opentute_addontag_entry_author($separator,$post->post_author);
					?>
				</div>
			</div>	
			<div class="col-md-3">
				&nbsp;
			</div>		
		</div>
	</section>		
</div>

<?php endwhile; wp_reset_query(); endif; endif; ?>
