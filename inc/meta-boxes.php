<?php
/**
 * OptionTree meta boxes
 */

add_action( 'admin_init', 'custom_meta_boxes' );

function custom_meta_boxes() {

    $opentute_addontag_post_meta_box = array(
        'id'        => 'opentute_addontag_post_meta_box',
        'title'     => 'Meta Options',
        'desc'      => '',
        'pages'     => array( 'post', 'page' ),
        'context'   => 'normal',
        'priority'  => 'high',
        'fields'    => array(
            array(
                'id'          => 'post_subtitle',
                'label'       => 'Optional Subtitle',
                'desc'        => 'This is an optional subtitle that will appear in single post or page under the title.',
                'std'         => '',
                'type'        => 'textarea-simple',
                'class'       => '',
                'choices'     => array()
            )
        )
    );
      
    ot_register_meta_box( $opentute_addontag_post_meta_box );
}